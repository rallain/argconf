# ArgConf

A multimodal arguments and configuration manager for your Node.JS apps

## Usage

```js
let conf = ArgConf({
    providers: {
        env: {priority: 1, settings: {prefix: 'test'}}, // 0 low priority, 999 high priority
        arg: {priority: 3, settings: {helper: true}},
        //s3: {priority: 5, settings: {region: 'eu-west-3', access_key: '', secret_key: '', bucket: '', object: ''}}
    },
    vars: {
        'client.persistent': {type: Boolean, required: false, default: true},
        'client.interface': {type: String, required: false, default: 'eth0'},
        'server.refresh.interval': {type: Number, required: true}
    }
});
```

| env | arg | default |
| --- | --- | --- |
| TEST_CLIENT_PERSISTENT | --client-persistent | true |
| TEST_CLIENT_INTERFACE | --client-interface | eth0 |
| TEST_SERVER_REFRESH_INTERVAL | --server-refresh-interval |  |
